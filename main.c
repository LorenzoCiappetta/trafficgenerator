#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>

#define PORT 9999
#define NUMBER_OF_NODES 10

int pipefd[2];//for IPC purposes, 'sibling' processes need to communicate 
int send_sock;

int my_id; //id of node
//what follows is an attempt to have the program close with grace
static int stop_signal_sent = 0;

void time_to_stop(int sig){
  if(sig == SIGUSR1){
      stop_signal_sent = 1;
  }
}

typedef struct Message {
    int id;
    int sequence;
    int payload;
} Message;

typedef struct Payload {
    int value;
} Payload;

Message * prepare_message(Payload * payload){
  payload -> value =(int) RAND_MAX / rand();
  Message* m = malloc(sizeof(Message));
  m -> payload = payload -> value;
  return m;
}

//---------------
// Traffic Analyzer
//---------------

typedef struct TrafficAnalyzerNode{
  struct TrafficAnalyzerNode* prev;
  struct TrafficAnalyzerNode* next;
  int source;
  time_t time_in_seconds;
}TrafficAnalyzerNode;

typedef struct TrafficAnalyzer {
    // Definition of sliding window...//double linked list, FIFO (remove the older element before the newer) 
    TrafficAnalyzerNode* head;
    TrafficAnalyzerNode* tail;
    int size;
} TrafficAnalyzer;

TrafficAnalyzer* traffic_analyzer_init(){
  TrafficAnalyzer* analyzer = malloc(sizeof(TrafficAnalyzer));
  analyzer -> head = NULL;
  analyzer -> tail = NULL;
  analyzer -> size = 0;
  return analyzer;
}

void traffic_analyzer_append(TrafficAnalyzer* analyzer, int source){
  if(analyzer == NULL){
    return;
  }
  TrafficAnalyzerNode* new = malloc(sizeof(TrafficAnalyzerNode));
  new -> source = source;
  new -> time_in_seconds = time(NULL);
  if(analyzer -> head == NULL){
    new -> prev = NULL;
    new -> next = NULL;
    analyzer -> head = new;
    analyzer -> tail = new;   
  }
  else{
    TrafficAnalyzerNode* last = analyzer -> tail;
    last -> next = new;
    new -> prev = last;
    new -> next = NULL;
    analyzer -> tail = new;
  }
  analyzer -> size++;
}

void traffic_analyzer_pop(TrafficAnalyzer* analyzer){
  if(analyzer == NULL || analyzer -> head == NULL){
    return;
  }
  TrafficAnalyzerNode* first = analyzer -> head;

  if(analyzer -> tail == first){
    analyzer -> head = NULL;
    analyzer -> tail = NULL;
    free(first);
  }
  else{
    TrafficAnalyzerNode* second = first -> next;

    analyzer -> head = second;
    second -> prev = NULL;

    free(first);
  }
  analyzer -> size--;
}

void traffic_analyzer_destroy(TrafficAnalyzer* analyzer){
  while(analyzer -> head != NULL){
    traffic_analyzer_pop(analyzer);
  }
  free(analyzer);
}

void received_pkt(TrafficAnalyzer * analyzer, int source) {
    // Record the packet send and datetime of it
    traffic_analyzer_append(analyzer, source);
    while(analyzer -> tail -> time_in_seconds - analyzer -> head -> time_in_seconds > 2){
      traffic_analyzer_pop(analyzer);
    }
}

void dump(TrafficAnalyzer * analyzer) {
    // Dump information about the thoughput of all packets received
    int traffic[NUMBER_OF_NODES] = {0};
    TrafficAnalyzerNode* current = analyzer -> head;
    while(current){
      traffic[current -> source]++;
      current = current -> next;
    }

    int i;

    for(i = 0; i < NUMBER_OF_NODES; i++){
        printf("%d: %03d, ", i, traffic[i]/2);//how many packets per 2 seconds / 2 = how many packets per second
    }
    printf("\n");
}

void traffic_analyzer_routine(){
  TrafficAnalyzer* analyzer = traffic_analyzer_init();
  time_t current_time;
  int ret;

  while(!stop_signal_sent){
    int id;//id of the sender

    ret = read(pipefd[0], (void*)&id, sizeof(int));
    if(ret == -1){
      if(errno != EINTR){
        printf("error: read() from pipe\n");
        exit(-1);
      }
    }

    received_pkt(analyzer, id);
    current_time = time(NULL);
    if(current_time % 2 == 0){//in theory this should dump every 1 second
      dump(analyzer);
    }
  }

  close(pipefd[0]);
  traffic_analyzer_destroy(analyzer);
}

void traffic_generator_routine(){
  int ret;
  int sequence = 0;

  socklen_t addr_len = sizeof(struct sockaddr_in);

  struct sockaddr_in broadcast_addr = {0};
  broadcast_addr.sin_family = AF_INET;
  broadcast_addr.sin_port = htons(PORT);
  broadcast_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

  Payload payload;
  while(!stop_signal_sent){
  //generate message
    Message* m = prepare_message(&payload);
    m -> sequence = sequence;
    sequence++;
    m -> id = my_id;
  //send message to all interfaces
    struct ifaddrs *addrs, *tmp;
    getifaddrs(&addrs);
    tmp = addrs;
    while (tmp){
      if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET) {
        setsockopt(send_sock, SOL_SOCKET, SO_BINDTODEVICE, tmp->ifa_name, sizeof(tmp->ifa_name));
        ret = sendto(send_sock, (void*)m, sizeof(Message), 0, (struct sockaddr*)&broadcast_addr, addr_len);
        if(ret == -1){
          printf("error: send\n");
          exit(-1);
        }
      }
      tmp = tmp->ifa_next;
    }
    freeifaddrs(addrs);
    free(m);
  //sleep 10ms  
    msleep(10);
  }
  close(send_sock);

}

void broadcaster_routine(){
  int ret;
  Message m;
  int id; //id of sender;

  while(!stop_signal_sent){
    //receive message
    struct ifaddrs *addrs, *tmp;
    getifaddrs(&addrs);
    tmp = addrs;
    while (tmp){
      if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET) {
        setsockopt(send_sock, SOL_SOCKET, SO_BINDTODEVICE, tmp->ifa_name, sizeof(tmp->ifa_name));
        ret = recvfrom(send_sock, (void*)&m, sizeof(Message), 0, NULL, NULL);
        if(ret == -1){
          if(errno != EINTR){
            printf("error: recv()\n");
            exit(-1);
          }
        }
        //send message id thru pipe
        id = m.id;
        if(id != my_id){
          ret = write(pipefd[1], (void*)&id, sizeof(int));
          if(ret == -1){
            printf("error: write() from pipe\n");
            exit(-1);
          }
        }
      }
      tmp = tmp->ifa_next;
    }
    freeifaddrs(addrs);
    
  }
  close(pipefd[1]);
  close(send_sock);
}
//-------------------------
// Utility
// ------------------------

/**
 * Bind the given socket to all interfaces (one by one)
 * and invoke the handler with same parameter
 */
/*void bind_to_all_interfaces(int sock, void * context, void (*handler)(int, void *)) {
    struct ifaddrs *addrs, *tmp;
    getifaddrs(&addrs);
    tmp = addrs;
    while (tmp){
        if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET) {
            setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, tmp->ifa_name, sizeof(tmp->ifa_name));
            handler(sock, context);
        }
        tmp = tmp->ifa_next;
    }
    freeifaddrs(addrs);
}*/

/**
 * Sleep a given amount of milliseconds
 */
int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do
    {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

int main(int argc, char* argv[]) {

  // Autoflush stdout for docker
  setvbuf(stdout, NULL, _IONBF, 0);

  int ret;
  int yes = 1;
  socklen_t addr_len = sizeof(struct sockaddr_in);
  my_id = atoi(argv[1]); 

  ret = pipe(pipefd);
  if(ret == -1){
    printf("error: pipe\n");
  }

  struct sigaction stop_signal;
  stop_signal.sa_handler = time_to_stop;
  sigaction(SIGUSR1, &stop_signal, NULL);

  struct sockaddr_in addr = {0};
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORT);
  addr.sin_addr.s_addr = htons(INADDR_ANY);

  send_sock = socket(AF_INET, SOCK_DGRAM, 0);
  if(0 > send_sock){
    printf("error: socket()\n");
    return -1;
  }

  ret = setsockopt(send_sock, SOL_SOCKET, SO_BROADCAST, (char*) &yes, sizeof(yes));
  if(0 > ret){
    printf("error: setsockopt()\n");
    return -1;
  }

  ret = bind(send_sock, (struct sockaddr*)&addr, addr_len);
  if(0 > ret){
    printf("error: bind()\n");
    return -1;
  }

  pid_t pid1;
  pid_t pid2;
  pid_t pid3;

  pid1 = fork();
  if(pid1 == -1){
    printf("error: fork");
  }
  if(pid1 == 0){
    // Traffic generator
    close(pipefd[0]);
    close(pipefd[1]);
    traffic_generator_routine();
    return 0;
  }

  pid2 = fork();
  if(pid2 == -1){
    printf("error: fork");
  }
  if(pid2 == 0){
    // Broadcaster
    close(pipefd[0]);
    broadcaster_routine();
    return 0;
  }

  pid3 = fork();
  if(pid3 == -1){
    printf("error: fork");
  }
  if(pid3 == 0){
    // Traffic analyzer
    close(send_sock);
    close(pipefd[1]);
    traffic_analyzer_routine();
    return 0;
  }

  close(pipefd[0]);
  close(pipefd[1]);

  sleep(20);//the simulation has to last 20s
  kill(pid1, SIGUSR1);
  kill(pid2, SIGUSR1);
  kill(pid3, SIGUSR1);

  int child;
  for(child = 0; child < 3; child++){
    wait(NULL);//we wait for child processes to end
  }

}